package geospatial

import (
	"fmt"
)

// ErrBadBoundingBox if the bounding box is not valid
var ErrBadBoundingBox = fmt.Errorf("Bouding box is not valid")

// ErrInvalidLatitude is returned if the latitude is invalid
var ErrInvalidLatitude = fmt.Errorf("Latitude is not valid")

// ErrInvalidLongitude is returned if the longitude is invalid
var ErrInvalidLongitude = fmt.Errorf("Longitude is not valid")

// Point is a geospatial point defined with longitude and latitude
type Point struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

// Within returns true if a point is in a given bounding box
func (p *Point) Within(bbox *Bbox) bool {
	lonInside := p.Lon <= bbox.NE.Lon && p.Lon >= bbox.SW.Lon
	latInside := p.Lat >= bbox.SW.Lat && p.Lat <= bbox.NE.Lat
	return lonInside && latInside
}

// NewPoint returns a new point
func NewPoint(lon, lat float64) (*Point, error) {
	err := validateLatitude(lat)
	if err != nil {
		return nil, err
	}

	err = validateLongitude(lon)
	if err != nil {
		return nil, err
	}

	return &Point{
		Lat: lat,
		Lon: lon,
	}, nil
}

// WayPoints is a list of points
type WayPoints []Point

// Bbox A bounding box (usually shortened to bbox) is an area defined by two
// longitudes and two latitudes.
//
//                       +-----------------+ North East Point (NE)
//                       |                 |
//                       |                 |
//                       |                 |
//                       |                 |
// South West Point (SW) +-----------------+
//
type Bbox struct {
	SW *Point `json:"sw"`
	NE *Point `json:"ne"`
}

func (b Bbox) String() string {
	return fmt.Sprintf("swlon: %f, swlat: %f - nelon: %f - nelat: %f", b.SW.Lon, b.SW.Lat, b.NE.Lon, b.NE.Lat)
}

// NewBbox returns a new Bbox or error if coordinates
func NewBbox(neLon, neLat, swLon, swLat float64) (*Bbox, error) {
	if swLon >= neLon || neLat <= swLat {
		return nil, ErrBadBoundingBox
	}

	ne, err := NewPoint(neLon, neLat)
	if err != nil {
		return nil, err
	}

	sw, err := NewPoint(swLon, swLat)
	if err != nil {
		return nil, err
	}

	return &Bbox{
		NE: ne,
		SW: sw,
	}, nil
}

// validateLatitude returns true is the value a a valid latitude
func validateLatitude(lat float64) error {
	if !(lat >= -90.0 && lat <= 90.0) {
		return ErrInvalidLatitude
	}
	return nil
}

// validateLongitude returns true is the value a a valid lonitude
func validateLongitude(lon float64) error {
	if !(lon >= -180.0 && lon <= 180.0) {
		return ErrInvalidLongitude
	}
	return nil
}
