package mock

import (
	"gitlab.com/alexjomin/dem/geospatial"
)

type Mock struct {
}

// New return a new Srtm1 Service
func New() (*Mock, error) {
	return &Mock{}, nil
}

// DownloadTileFromLonLat
func (m *Mock) DownloadTileFromLonLat(p *geospatial.Point) error {
	return nil
}

// DownloadTilesFromBoundingBox
func (m *Mock) DownloadTilesFromBoundingBox(bbox *geospatial.Bbox) error {
	return nil
}

func (m *Mock) ExtractElevationFromLonLat(p *geospatial.Point) (elevation int16, err error) {
	return
}
