package srtmgl1

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"os"
)

const tileExtension = ".hgt"
const zipExtension = ".zip"

const filenameSuffix = "SRTMGL1"
const tileBytesSize = 25934402
const voidElevation = -32768
const gridSize = 3601
const gridResolution = 1.0 / gridSize

// ErrVoidElevation is returned when the tile contains a void elevation
var ErrVoidElevation = errors.New("DEM does not contains elevation")

// ErrTileWrongSize is returned when a tile does not have a correct size
var ErrTileWrongSize = fmt.Errorf("SRTMGL1 Tile must contain %d bytes", tileBytesSize)

// Tile structur
type Tile struct {
	// Name of tile like N50E003.SRTMGL1
	Name string
	// Filename is the final unzipped filename N50E003.hgt
	Filename string
	// ZippedFilename N50E003.SRTMGL1.hgt.zip
	ZippedFilename string
	// Lon Initial longitude of the point
	Lon float64
	// Lat Initial latitude of the point
	Lat float64
	// Data Raw data of the tile
	Data []byte
	// Zipped Data of the file
	ZippedData []byte
}

// Tiles is a collection of Tile
type Tiles []Tile

// getURL returns the URL to download the tile
func (t *Tile) getURL() string {
	return fmt.Sprintf("%s/%s", srtmURL, t.ZippedFilename)
}

// SaveAt persists the tile to a file
func (t *Tile) saveAt(path string) error {
	f, err := os.Create(path + "/" + t.Filename)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.Write(t.Data)

	// TODO refactor
	// Flush data as have persisted it on disk
	if err != nil {
		t.Data = nil
	}
	return err
}

// NewTileFromLonLat returns a tile for a given longitude and latitude
func NewTileFromLonLat(lon, lat float64) Tile {
	name := getTileNameFromLonLat(lon, lat)
	tile := Tile{
		Name:           name,
		Lon:            lon,
		Lat:            lat,
		Filename:       name + tileExtension,
		ZippedFilename: name + "." + filenameSuffix + tileExtension + zipExtension,
	}
	return tile
}

// NewTilesFromBoundingBox returns a set of Tiles corresponfing to the
// resulting matrix of splitting the bounding box by 1x1 sized tile
func NewTilesFromBoundingBox(neLon, neLat, swLon, swLat float64) (Tiles, error) {
	// if swLon >= neLon || neLat <= swLat {
	// 	return nil, ErrBadBoundingBox
	// }

	tiles := Tiles{}
	floorLon := math.Floor(swLon)
	ceilLon := math.Ceil(neLon)
	floorLat := math.Floor(swLat)
	ceilLat := math.Ceil(neLat)

	for i := floorLon; i <= ceilLon; i++ {
		for j := floorLat; j <= ceilLat; j++ {
			tile := NewTileFromLonLat(i, j)
			tiles = append(tiles, tile)
		}
	}
	return tiles, nil
}

// getTileNameFromLonLat
// As the spatial extent is form 60°N to 56°S, and from 180°W to 180°E
// We need to convert longitude and latitude into the good cardinal point
func getTileNameFromLonLat(lon, lat float64) string {
	ns := "N"
	latOffset := 0
	if lat < 0 {
		ns = "S"
		latOffset = 1
	}

	ew := "E"
	lonOffset := 0
	if lon < 0 {
		ew = "W"
		lonOffset = 1
	}
	truncatedLat := int(math.Abs(lat)) + latOffset
	truncatedLon := int(math.Abs(lon)) + lonOffset
	return fmt.Sprintf("%s%02d%s%03d", ns, truncatedLat, ew, truncatedLon)
}

func intAbs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func getElevationPositionFromLonLat(lon, lat float64) int {
	row := int((math.Floor(lat) + 1.0 - lat) * (float64(gridSize - 1.0)))
	column := int((lon - math.Floor(lon)) * (float64(gridSize - 1.0)))
	index := row*gridSize + column
	return index
}

// readValue read elevation as a big endian 16 bits integer
func readValue(data []byte) (ret int16) {
	buf := bytes.NewBuffer(data)
	binary.Read(buf, binary.BigEndian, &ret)
	return
}
