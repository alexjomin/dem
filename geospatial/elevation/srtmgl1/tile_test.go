package srtmgl1

import (
	"reflect"
	"testing"
)

func Test_getTileNameFromLonLat(t *testing.T) {
	type args struct {
		lon float64
		lat float64
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test Decimal",
			args{
				lon: 100.50,
				lat: 50.6877,
			},
			"N50E100",
		},
		{
			"Test Int",
			args{
				lon: 1,
				lat: 5,
			},
			"N05E001",
		},
		{
			"Negative lat value",
			args{
				lon: 133.69,
				lat: -27.65,
			},
			"S28E133",
		},
		{
			"Negative lon value",
			args{
				lon: -125.67,
				lat: 52.68,
			},
			"N52W126",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getTileNameFromLonLat(tt.args.lon, tt.args.lat); got != tt.want {
				t.Errorf("getTileNameFronLonLat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetTilesFromBoundingBox(t *testing.T) {
	type args struct {
		swLon float64
		swLat float64
		neLon float64
		neLat float64
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			"Simple Bbox",
			args{
				2,
				41,
				1,
				40,
			},
			[]string{"N40E001", "N41E001", "N40E002", "N41E002"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tiles, err := NewTilesFromBoundingBox(tt.args.swLon, tt.args.swLat, tt.args.neLon, tt.args.neLat)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTilesFromBoundingBox() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			names := []string{}

			for _, tile := range tiles {
				names = append(names, tile.Name)
			}
			if !reflect.DeepEqual(names, tt.want) {
				t.Errorf("GetTilesFromBoundingBox() = %v, want %v", names, tt.want)
			}
		})
	}
}

func TestNewTileFromLonLat(t *testing.T) {
	type args struct {
		lon float64
		lat float64
	}
	tests := []struct {
		name string
		args args
		want Tile
	}{
		{
			"Simple test",
			args{
				lon: 100.50,
				lat: 50.6877,
			},
			Tile{
				Name:           "N50E100",
				Filename:       "N50E100" + tileExtension,
				ZippedFilename: "N50E100." + filenameSuffix + tileExtension + zipExtension,
				Lon:            100.50,
				Lat:            50.6877,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewTileFromLonLat(tt.args.lon, tt.args.lat); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewTileFromLonLat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTile_getURL(t *testing.T) {
	type fields struct {
		Name           string
		Filename       string
		ZippedFilename string
		Extension      string
		Lon            float64
		Lat            float64
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			"Basic Test",
			fields{
				Name:           "N50E100",
				Extension:      tileExtension,
				Filename:       "N50E100" + tileExtension,
				ZippedFilename: "N50E100" + "." + filenameSuffix + tileExtension + zipExtension,
				Lon:            100.50,
				Lat:            50.6877,
			},
			"https://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/N50E100.SRTMGL1.hgt.zip",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tile := &Tile{
				Name:           tt.fields.Name,
				Filename:       tt.fields.Filename,
				ZippedFilename: tt.fields.ZippedFilename,
				Lon:            tt.fields.Lon,
				Lat:            tt.fields.Lat,
			}
			if got := tile.getURL(); got != tt.want {
				t.Errorf("Tile.getURL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewTilesFromBoundingBox(t *testing.T) {
	type args struct {
		swLon float64
		swLat float64
		neLon float64
		neLat float64
	}
	tests := []struct {
		name    string
		args    args
		want    Tiles
		wantErr bool
	}{
		{
			"Simple Bbox",
			args{
				1,
				40,
				2,
				41,
			},
			Tiles{
				{
					Name:           "N40E001",
					Lon:            1,
					Lat:            40,
					Filename:       "N40E001" + tileExtension,
					ZippedFilename: "N40E001" + "." + filenameSuffix + tileExtension + zipExtension,
				},
				{
					Name:           "N41E001",
					Lon:            1,
					Lat:            41,
					Filename:       "N41E001" + tileExtension,
					ZippedFilename: "N41E001" + "." + filenameSuffix + tileExtension + zipExtension,
				},
				{
					Name:           "N40E002",
					Lon:            2,
					Lat:            40,
					Filename:       "N40E002" + tileExtension,
					ZippedFilename: "N40E002" + "." + filenameSuffix + tileExtension + zipExtension,
				},
				{
					Name:           "N41E002",
					Lon:            2,
					Lat:            41,
					Filename:       "N41E002" + tileExtension,
					ZippedFilename: "N41E002" + "." + filenameSuffix + tileExtension + zipExtension,
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewTilesFromBoundingBox(tt.args.neLon, tt.args.neLat, tt.args.swLon, tt.args.swLat)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewTilesFromBoundingBox() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewTilesFromBoundingBox() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getElevationPositionFromLonLat(t *testing.T) {
	type args struct {
		lon float64
		lat float64
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Basic Test",
			args{
				1.0,
				50,
			},
			12963600,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getElevationPositionFromLonLat(tt.args.lon, tt.args.lat)
			if got != tt.want {
				t.Errorf("getElevationPositionFromLonLat() = %v, want %v", got, tt.want)
			}
			if got > tileBytesSize {
				t.Errorf("getElevationPositionFromLonLat() = %v, is greater than %v", got, tileBytesSize)
			}
		})
	}
}

func Test_value(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		args    args
		wantRet int16
	}{
		{
			"basic",
			args{
				[]byte{0x0, 0x1},
			},
			1,
		},
		{
			"basic 2",
			args{
				[]byte{0x1, 0x1},
			},
			257,
		},
		{
			"basic 3",
			args{
				[]byte{18, 160},
			},
			4768,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRet := readValue(tt.args.data); gotRet != tt.wantRet {
				t.Errorf("readValue() = %v, want %v", gotRet, tt.wantRet)
			}
		})
	}
}
