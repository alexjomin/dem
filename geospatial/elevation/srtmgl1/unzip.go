package srtmgl1

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"log"
)

// Unzip TODO refactor !
func (t *Tile) unzip() {
	size := int64(len(t.ZippedData))
	r, err := zip.NewReader(bytes.NewReader(t.ZippedData), size)

	if err != nil {
		log.Panic(err)
	}

	for _, f := range r.File {
		d, err := f.Open()
		if err != nil {
			log.Panic(err)
		}

		content, err := ioutil.ReadAll(d)

		if err != nil {
			log.Panic(err)
		}

		t.Data = content
		t.Filename = f.Name
	}
}
