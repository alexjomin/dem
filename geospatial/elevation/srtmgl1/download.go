package srtmgl1

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func (t *Tile) download(HTTPClient *http.Client) error {
	url := t.getURL()
	log.Printf("Downloading tile at %s", url)
	req, err := http.NewRequest("GET", url, nil)
	resp, err := HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %d - %s", resp.StatusCode, resp.Status)
	}

	content, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}

	filetype := http.DetectContentType(content)

	switch filetype {
	case "application/x-gzip", "application/zip":
		t.ZippedData = content
	default:
		t.Data = content
	}

	return nil
}
