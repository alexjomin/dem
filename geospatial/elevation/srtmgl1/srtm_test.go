package srtmgl1

import (
	"testing"

	"gitlab.com/alexjomin/dem/geospatial"
)

func BenchmarkSrtm1_ExtractElevationFromLonLat(b *testing.B) {
	s, err := New("./testdata", "", "")

	if err != nil {
		b.Log(err)
		b.Fail()
	}

	p, err := geospatial.NewPoint(6.865175, 45.8329)

	if err != nil {
		b.Log(err)
		b.Fail()
	}

	// Testing with Mont Blanc lon lat
	// The tile is already in the testdata folder
	for n := 0; n < b.N; n++ {
		s.ExtractElevationFromLonLat(p)
	}
}

func TestSrtm1_ExtractElevationFromLonLat(t *testing.T) {
	s, err := New("./testdata", "", "")

	if err != nil {
		t.Log(err)
		t.Fail()
	}

	p, err := geospatial.NewPoint(6.865175, 45.8329)

	if err != nil {
		t.Log(err)
		t.Fail()
	}

	ele, err := s.ExtractElevationFromLonLat(p)

	if err != nil {
		t.Log(err)
		t.Fail()
	}

	if ele != 4788 {
		t.Logf("Expected elevation is 4788, found %d", ele)
		t.Fail()
	}
}
