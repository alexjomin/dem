package srtmgl1

import (
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"

	"gitlab.com/alexjomin/dem/geospatial"
)

// The Shuttle Radar Topography Mission (SRTM) (Wikipedia article) is a NASA
// mission conducted in 2000 to obtain elevation data for most of the world. It
// is the current dataset of choice for digital elevation model (DEM) data
// since it has a fairly high resolution (1 arc-second, or around 25 meters),
// has near-global coverage (from 56°S to 60°N), and is in the public domain.
//
// The official 3-arc-second and 1-arc-second data for versions 2.1 and 3.0 are
// divided into 1°×1° data tiles. The tiles are distributed as zip files
// containing HGT files labeled with the coordinate of the southwest cell. For
// example, the file N20E100.hgt contains data from 20°N to 21°N and from 100°E
// to 101°E inclusive.
//
// The HGT files have a very simple format. Each file is a series of 16-bit
// integers giving the height of each cell in meters arranged from west to east
// and then north to south.
//
// Each 3-arc-second data tile has 1442401 integers representing a 1201×1201
// grid, while each 1-arc-second data tile has 12967201 integers representing a
// 3601×3601 grid. The outermost rows and columns of each tile overlap with the
// corresponding rows and columns of adjacent tiles.
//
// More info:
// https://fr.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission
// https://wiki.openstreetmap.org/wiki/SRTM
// https://gis.stackexchange.com/questions/43743/extracting-elevation-from-hgt-file
// https://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/
// https://dds.cr.usgs.gov/srtm/version2_1/Documentation/SRTM_Topo.pdf

// Host to download SRTM1 tiles
const srtmURL = "https://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11"

// Srtm1 is the service used for download tile from the SRTM1 Terrai model
// Data comes from NASA’s Earth Observing System Data and Information System (EOSDIS)
// To obtain a NASA Earthdata Login account, please visit:
// https://urs.earthdata.nasa.gov/users/new
type Srtm1 struct {
	Path       string
	HTTPClient *http.Client
}

// New return a new Srtm1 Service
func New(path, login, password string) (*Srtm1, error) {
	options := cookiejar.Options{}
	cookieJar, _ := cookiejar.New(&options)
	httpClient := &http.Client{
		Jar: cookieJar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			// only needed in "https://urs.earthdata.nasa.gov/oauth/authorize"
			req.SetBasicAuth(login, password)
			return nil
		},
	}

	return &Srtm1{
		Path:       path,
		HTTPClient: httpClient,
	}, nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DownloadTileFromLonLat
func (s *Srtm1) DownloadTileFromLonLat(p *geospatial.Point) error {

	t := NewTileFromLonLat(p.Lon, p.Lat)

	if !fileExists(s.Path + "/" + t.Filename) {
		err := t.download(s.HTTPClient)
		if err != nil {
			return err
		}
		t.unzip()
		t.saveAt(s.Path)
	} else {
		fmt.Println("File already exists: ", t.Filename)
	}

	return nil
}

// DownloadTilesFromBoundingBox
func (s *Srtm1) DownloadTilesFromBoundingBox(bbox *geospatial.Bbox) error {
	tiles, err := NewTilesFromBoundingBox(bbox.NE.Lon, bbox.NE.Lat, bbox.SW.Lon, bbox.SW.Lat)

	if err != nil {
		return err
	}

	for _, t := range tiles {
		if !fileExists(s.Path + "/" + t.Filename) {
			err := t.download(s.HTTPClient)
			if err != nil {
				continue
			}
			t.unzip()
			t.saveAt(s.Path)
		} else {
			fmt.Println("File already exists: ", t.Filename)
		}
	}

	return nil
}

// ExtractElevationFromLonLat returns elevation from tile Height files have the
// extension .HGT and are signed two byte integers. The bytes are in Motorola
// "big-endian" order with the most significant byte first. Heights are in
// meters referenced to the WGS84/EGM96 geoid. Data voids are assigned the value
// -32768.
func (s *Srtm1) ExtractElevationFromLonLat(p *geospatial.Point) (elevation int16, err error) {
	t := NewTileFromLonLat(p.Lon, p.Lat)

	if !fileExists(s.Path + "/" + t.Filename) {
		err = t.download(s.HTTPClient)
		if err != nil {
			return
		}
		t.unzip()
		t.saveAt(s.Path)
	}

	file, err := os.Open(s.Path + "/" + t.Filename)

	if err != nil {
		return elevation, err
	}

	defer file.Close()

	i := getElevationPositionFromLonLat(p.Lon, p.Lat)

	sr := io.NewSectionReader(file, int64(i*2), 2)
	err = binary.Read(sr, binary.BigEndian, &elevation)

	if err != nil {
		return elevation, err
	}

	if elevation == voidElevation {
		err = ErrVoidElevation
		log.Printf("Void elevation at lon: %d, lat: %d", 0, 0)
	}

	return
}
