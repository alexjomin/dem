package elevation

import "gitlab.com/alexjomin/dem/geospatial"

type Model interface {
	DownloadTileFromLonLat(p *geospatial.Point) error
	DownloadTilesFromBoundingBox(bbox *geospatial.Bbox) error
	ExtractElevationFromLonLat(p *geospatial.Point) (elevation int16, err error)
}
