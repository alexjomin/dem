package geospatial

import (
	"reflect"
	"testing"
)

func TestNewPoint(t *testing.T) {
	type args struct {
		lon float64
		lat float64
	}
	tests := []struct {
		name    string
		args    args
		want    *Point
		wantErr bool
	}{
		{
			"good",
			args{
				lon: 3.05,
				lat: 50.0132,
			},
			&Point{
				Lon: 3.05,
				Lat: 50.0132,
			},
			false,
		},
		{
			"bad lat",
			args{
				lon: 5.01,
				lat: 150.0132,
			},
			nil,
			true,
		},
		{
			"bad lon",
			args{
				lon: 1000.01,
				lat: 50.0132,
			},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewPoint(tt.args.lon, tt.args.lat)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewPoint() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPoint() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewBbox(t *testing.T) {
	type args struct {
		neLon float64
		neLat float64
		swLon float64
		swLat float64
	}
	tests := []struct {
		name    string
		args    args
		want    *Bbox
		wantErr bool
	}{
		{
			"good",
			args{
				neLon: 3.05,
				neLat: 50.0132,
				swLon: 2.501,
				swLat: 49.001,
			},
			&Bbox{
				SW: &Point{
					Lat: 49.001,
					Lon: 2.501,
				},
				NE: &Point{
					Lon: 3.05,
					Lat: 50.0132,
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewBbox(tt.args.neLon, tt.args.neLat, tt.args.swLon, tt.args.swLat)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewBbox() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBbox() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_validateLatitude(t *testing.T) {
	type args struct {
		lat float64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"good",
			args{
				50.001,
			},
			false,
		},
		{
			"good lat lower limit",
			args{
				-90.000,
			},
			false,
		},
		{
			"good lat upper limit",
			args{
				90.000,
			},
			false,
		},
		{
			"wrong lat over range",
			args{
				91.001,
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateLatitude(tt.args.lat); (err != nil) != tt.wantErr {
				t.Errorf("validateLatitude() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_validateLongitude(t *testing.T) {
	type args struct {
		lon float64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"good",
			args{
				50.001,
			},
			false,
		},
		{
			"good lat lower limit",
			args{
				-180.000,
			},
			false,
		},
		{
			"good lat upper limit",
			args{
				180.000,
			},
			false,
		},
		{
			"wrong lat over range",
			args{
				181.001,
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validateLongitude(tt.args.lon); (err != nil) != tt.wantErr {
				t.Errorf("validateLongitude() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPoint_Within(t *testing.T) {

	france, err := NewBbox(8.964844, 51.740636, -6.767578, 42.212245)

	if err != nil {
		t.Errorf("Error during creating a bbox - %s", err)
	}

	type fields struct {
		Lon float64
		Lat float64
	}
	type args struct {
		bbox *Bbox
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			"inside",
			fields{
				2.131,
				47.413,
			},
			args{
				france,
			},
			true,
		},
		{
			"outside",
			fields{
				-0.41,
				52.442,
			},
			args{
				france,
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Point{
				Lon: tt.fields.Lon,
				Lat: tt.fields.Lat,
			}
			if got := p.Within(tt.args.bbox); got != tt.want {
				t.Errorf("Point.Within() = %v, want %v", got, tt.want)
			}
		})
	}
}
