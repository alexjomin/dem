package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/alexjomin/dem/geospatial"
)

var bboxForTiles *geospatial.Bbox

// tilesCmd represents the tiles command
var tilesCmd = &cobra.Command{
	Use:     "tiles",
	Short:   "Downloads a matrix of tiles corresponding to the given bounding box",
	Long:    `Bounding box is defined by the upper point aka North East point and the lower point aka South West point`,
	Example: "dem tiles  --login=$LOGIN --password=$PASSOWRD -- 4.71 45.870 4.644 45.847",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 4 {
			cmd.Help()
			os.Exit(0)
		}

		// North East Point
		neLon, err := convertToFloat(args[0])
		if err != nil {
			fmt.Println(Red("NE Longitude is not valid"))
			os.Exit(1)
		}

		neLat, err := convertToFloat(args[1])
		for err != nil {
			fmt.Println(Red("NE Latitude is not valid"))
			os.Exit(1)
		}

		// South West Point
		swLon, err := convertToFloat(args[2])
		if err != nil {
			fmt.Println(Red("SW Longitude is not valid"))
			os.Exit(1)
		}

		swLat, err := convertToFloat(args[3])
		if err != nil {
			fmt.Println(Red("SW Latitude is not valid"))
			os.Exit(1)
		}

		bboxForTiles, err = geospatial.NewBbox(neLon, neLat, swLon, swLat)

		if err != nil {
			fmt.Println(Red(err))
			os.Exit(1)
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		m.DownloadTilesFromBoundingBox(bboxForTiles)
	},
}

func init() {
	rootCmd.AddCommand(tilesCmd)
}
