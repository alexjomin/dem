/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"io/ioutil"
	"os"
	"regexp"
	"text/template"

	"github.com/spf13/cobra"
)

// readmeCmd represents the readme command
var readmeCmd = &cobra.Command{
	Use:   "readme",
	Short: "Generate Readme",

	Run: func(cmd *cobra.Command, args []string) {
		rootCmd := cmd.Parent()
		type ReadmeData struct {
			Rootcmd  map[string]string
			Commands []map[string]interface{}
		}
		sweaters := ReadmeData{
			Rootcmd: map[string]string{
				"name":        rootCmd.Name(),
				"short":       rootCmd.Short,
				"description": rootCmd.Long,
			},
		}

		for _, cmd := range rootCmd.Commands() {
			cmd.HasAvailableFlags()
			cmd := map[string]interface{}{
				"name":        cmd.Name(),
				"example":     cmd.Example,
				"description": cmd.Long,
				"cmd":         cmd,
				"short":       cmd.Short,
			}
			sweaters.Commands = append(sweaters.Commands, cmd)
		}

		filedata, err := ioutil.ReadFile("./readme.tpl")

		if err != nil {
			panic(err)
		}
		tmpl, err := template.New("test").Funcs(template.FuncMap{
			"trim": func(s string) string {
				reg := regexp.MustCompile(`(?m)^[\s\t]{1,}`)
				return reg.ReplaceAllString(s, "${1}")
			},
		}).Parse(string(filedata))
		if err != nil {
			panic(err)
		}

		fi, err := os.Create("./README.md")
		if err != nil {
			panic(err)
		}

		defer fi.Close()

		err = tmpl.Execute(fi, sweaters)
		if err != nil {
			panic(err)
		}
	},
}

func init() {
	rootCmd.AddCommand(readmeCmd)

}
