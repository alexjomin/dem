package cmd

import (
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/alexjomin/dem/geospatial/elevation"
	"gitlab.com/alexjomin/dem/geospatial/elevation/srtmgl1"
)

// args
var (
	// global args
	login     string
	password  string
	modelName string
	path      string
)

var m elevation.Model

const modelSrtm1 = "srtmgl1"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "dem",
	Short: "Digital Elevation Model Tool",
	Long: `**dem** is a cli tool to deal with Digital Elevation Model written in Go.

DEMs are used often in geographic information systems as a 3D representation of a terrain's surface, commonly of a planet.

There are a bunch of DEMs available: 
- SRTM
- ASTER
- JAXA’s Global ALOS 3D World
- LIDAR
- etc.

For the SRTM (Shuttle Radar Topography Mission) The elevation models are arranged into tiles, each covering one degree of latitude and one degree of longitude,  named according to their south western corners. 
For example, "n45e006" stretches from 45°N 6°E to 46°N 7°E and "s45w006" from 45°S 6°W to 44°S 5°W

Actually only the SRTMGL1 (SRTM Version 3.0 Global 1 arc second) is implemented. 
1 arc second resolution offer a precision of about 30 meters. For more information see: 
https://earthdata.nasa.gov/learn/articles/nasa-shuttle-radar-topography-mission-srtm-version-3-0-global-1-arc-second-data-released-over-asia-and-australia

Data comes from NASA’s Earth Observing System Data and Information System (EOSDIS)

To obtain a NASA Earthdata Login account, please visit: https://urs.earthdata.nasa.gov/users/new

This purpose of this cli is to provide commands to: 
- Easely download: 
	- A tile from longitude and latitude 
	- A matrix of tiles from a bounding box defined by a North East and a South West points.
- Create a HTTP server to return the elevation in meter:
	- [GET] /srtmgl1/elevation?lon=3.00&lat=50.00\
	- [POST] /srtmgl1/elevations\
		-  ex: curl -X POST https://myhost.com/srtmgl1/elevations -H "Content-Type: application/json" -d '[{"lon":2.71,"lat":50.32}]'
`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(Red(err))
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&login, "login", "", "Login for the srtm service")
	rootCmd.PersistentFlags().StringVar(&password, "password", "", "Password for the srtm service")
	rootCmd.PersistentFlags().StringVar(&modelName, "model", "srtmgl1", "Terrain Model used")
	rootCmd.PersistentFlags().StringVar(&path, "path", ".", "Path where data will be downloaded")
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if modelName == modelSrtm1 {
		var err error
		m, err = srtmgl1.New(path, login, password)
		if err != nil {
			fmt.Println(Red(err))
			os.Exit(1)
		}
	} else {
		fmt.Printf(Red("Model `%s` not implemented\n"), modelName)
		os.Exit(1)
	}

	rootCmd.MarkFlagRequired("login")
	rootCmd.MarkFlagRequired("password")

	fmt.Printf(Green("Model is set to '%s', files will be downloaded to '%s'\n"), modelName, path)
}

// ConvertToFloat converts a string to a float64
func convertToFloat(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}
