package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/alexjomin/dem/geospatial"
)

var pointForTile *geospatial.Point

// tileCmd represents the tile command
var tileCmd = &cobra.Command{
	Use:     "tile",
	Short:   "Downloads the tile corresponding on the given longitude and latitude",
	Example: "dem tile --login=$LOGIN --password=$PASSOWRD -- 3.0004 50.772",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 2 {
			cmd.Help()
			os.Exit(0)
		}
		var err error
		lon, err := convertToFloat(args[0])
		if err != nil {
			fmt.Println(Red("Longitude is not valid"))
			os.Exit(1)
		}

		lat, err := convertToFloat(args[1])
		if err != nil {
			fmt.Println(Red("Latitude is not valid"))
			os.Exit(1)
		}

		pointForTile, err = geospatial.NewPoint(lon, lat)

		if err != nil {
			fmt.Println(Red(err))
			os.Exit(1)
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		m.DownloadTileFromLonLat(pointForTile)
	},
}

func init() {
	rootCmd.AddCommand(tileCmd)
}
