package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/alexjomin/dem/geospatial"
)

var pointForElevation *geospatial.Point

// tileCmd represents the tile command
var elevationCmd = &cobra.Command{
	Use:     "elevation",
	Short:   "Returns the elevation in meter corresponding to the given longitude and latitude",
	Example: "dem elevation  --login=$LOGIN --password=$PASSOWRD -- 3.0004 50.772",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 2 {
			cmd.Help()
			os.Exit(0)
		}
		var err error
		lon, err := convertToFloat(args[0])
		if err != nil {
			fmt.Println(Red("Longitude is not valid"))
			os.Exit(1)
		}

		lat, err := convertToFloat(args[1])
		if err != nil {
			fmt.Println(Red("Latitude is not valid"))
			os.Exit(1)
		}

		pointForElevation, err = geospatial.NewPoint(lon, lat)

		if err != nil {
			fmt.Println(Red(err))
			os.Exit(1)
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		ele, err := m.ExtractElevationFromLonLat(pointForElevation)
		if err != nil {
			fmt.Println(Red(err))
		}
		fmt.Println(ele)
	},
}

func init() {
	rootCmd.AddCommand(elevationCmd)
}
