package cmd

import (
	"fmt"
	"net/http"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/alexjomin/dem/geospatial"
	"gitlab.com/alexjomin/dem/handler"
)

var elevationAPI handler.API
var apiPort = "8000"
var bboxTool = false

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Create a HTTP server",
	Long: `Provides 2 endpoints to get elevation based on longitude and latitude: 
- GET /srtmgl1/elevation?lon=3.00&lat=50.00
- POST /srtmgl1/elevations
	-  ex: curl -X POST https://myhost.com/srtmgl1/elevations -H "Content-Type: application/json" -d '[{"lon":2.71,"lat":50.32}]'

The server is started within a bounding box, for instance 4.5153 50.9584 1.4941 49.7528, 
respectively sw-lon, sw-lat, ne-lon, ne-lat. So needed tiles are downloaded before the server starts to limit the latency.`,
	Example: "dem serve --login=$LOGIN --password=$PASSWORD -- 4.5153 50.9584 1.4941 49.7528",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 4 {
			cmd.Help()
			os.Exit(0)
		}
		// North East Point
		neLon, err := convertToFloat(args[0])
		if err != nil {
			fmt.Println(Red("NE Longitude is not valid"))
			os.Exit(1)
		}

		neLat, err := convertToFloat(args[1])
		for err != nil {
			fmt.Println(Red("NE Latitude is not valid"))
			os.Exit(1)
		}

		// South West Point
		swLon, err := convertToFloat(args[2])
		if err != nil {
			fmt.Println(Red("SW Longitude is not valid"))
			os.Exit(1)
		}

		swLat, err := convertToFloat(args[3])
		if err != nil {
			fmt.Println(Red("SW Latitude is not valid"))
			os.Exit(1)
		}

		bbox, err := geospatial.NewBbox(neLon, neLat, swLon, swLat)

		if err != nil {
			fmt.Println(Red(err))
			os.Exit(1)
		}

		elevationAPI = handler.API{
			Bbox:           bbox,
			ElevationModel: m,
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		// Downloading needed tiles for the bounding box
		fmt.Println(Green("Downloading needed tiles"))
		m.DownloadTilesFromBoundingBox(elevationAPI.Bbox)

		// Setup handlers
		http.HandleFunc("/srtmgl1/elevation", elevationAPI.GetElevation)
		http.HandleFunc("/srtmgl1/elevations", elevationAPI.ElevationProfile)
		http.HandleFunc("/bounding-box", elevationAPI.GetBoundingBox)

		if bboxTool {
			fmt.Println(Yellow("Starting bbox tool /tool"))
			fs := http.FileServer(http.Dir("./tool"))
			http.Handle("/tool/", http.StripPrefix("/tool/", fs))
		}

		fmt.Println(Green("Starting server"))

		// Start Server
		http.ListenAndServe(":"+apiPort, nil)
	},
}

func init() {
	serveCmd.Flags().StringVarP(&apiPort, "port", "p", "8080", "The port of the HTTP Server")
	serveCmd.Flags().BoolVarP(&bboxTool, "tool", "t", false, "Start the bounding box tool ")
	rootCmd.AddCommand(serveCmd)
}
