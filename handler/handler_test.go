package handler

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/alexjomin/dem/geospatial"
	"gitlab.com/alexjomin/dem/geospatial/elevation/mock"
)

type pairs map[string]string

var testBbox, _ = geospatial.NewBbox(8.964844, 51.740636, -6.767578, 42.212245)
var outOfBbox, _ = geospatial.NewPoint(9.0, 50)

func initAPI() API {
	mock, _ := mock.New()
	return API{
		Bbox:           testBbox,
		ElevationModel: mock,
	}
}

func buildQuery(r *http.Request, p pairs) {
	q := r.URL.Query()
	for k, v := range p {
		q.Add(k, v)
	}
	r.URL.RawQuery = q.Encode()
}

func Test_getElevation(t *testing.T) {
	api := initAPI()

	type test struct {
		name         string
		pairs        pairs
		expectedCode int
	}

	tests := []test{
		{
			"good",
			pairs{
				"lon": "3.58",
				"lat": "47.413",
			},
			http.StatusOK,
		},
		{
			"wrong missing lon",
			pairs{
				"lat": "47.413",
			},
			http.StatusBadRequest,
		},
		{
			"wrong missing lat",
			pairs{
				"lon": "47.413",
			},
			http.StatusBadRequest,
		},
		{
			"wrong missing lon and lat",
			pairs{},
			http.StatusBadRequest,
		},
		{
			"wrong crappy lon",
			pairs{
				"lon": "foo",
				"lat": "47.413",
			},
			http.StatusBadRequest,
		},
		{
			"wrong crappy lat",
			pairs{
				"lat": "foo",
				"lon": "47.413",
			},
			http.StatusBadRequest,
		},
		{
			"outof bbox",
			pairs{
				"lat": "41",
				"lon": "9.0",
			},
			http.StatusRequestedRangeNotSatisfiable,
		},
	}

	handler := http.HandlerFunc(api.GetElevation)

	for _, testcase := range tests {
		t.Run(testcase.name, func(t *testing.T) {

			// flush does'nt seems to work, so let's recreate recorder every time
			rr := httptest.NewRecorder()
			req, err := http.NewRequest("GET", "/foo-bar", nil)

			if err != nil {
				t.Fatal(err)
			}

			buildQuery(req, testcase.pairs)
			handler.ServeHTTP(rr, req)

			// Check the status code is what we expect.
			if status := rr.Code; status != testcase.expectedCode {
				t.Errorf("handler returned wrong status code: got %v want %v - %s", status, testcase.expectedCode, rr.Body)
			}
		})
	}

}

func Test_GetBoundingBox(t *testing.T) {
	api := initAPI()

	handler := http.HandlerFunc(api.GetBoundingBox)

	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/foo-bar", nil)

	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	content, err := ioutil.ReadAll(rr.Body)

	if err != nil {
		t.Fatal(err)
	}

	b := geospatial.Bbox{}
	json.Unmarshal(content, &b)

	if !reflect.DeepEqual(*testBbox, b) {
		t.Errorf("Bounding box not equal %s || %s", b, testBbox)
	}

}

func TestAPI_ElevationProfile(t *testing.T) {
	api := initAPI()

	type test struct {
		name         string
		verb         string
		pairs        pairs
		payload      []byte
		expectedCode int
	}

	tests := []test{
		{
			"good",
			http.MethodPost,
			pairs{},
			[]byte(`[{"lon":2.71,"lat":50.32}]`),
			http.StatusOK,
		},
		{
			"wrong bad verb",
			http.MethodPut,
			pairs{},
			nil,
			http.StatusMethodNotAllowed,
		},
		{
			"wrong bad payload",
			http.MethodPost,
			pairs{},
			nil,
			http.StatusBadRequest,
		},
		{
			"out of bounds",
			http.MethodPost,
			pairs{},
			[]byte(`[{"lon":-8.71,"lat":50.32}]`),
			http.StatusRequestedRangeNotSatisfiable,
		},
	}

	handler := http.HandlerFunc(api.ElevationProfile)

	for _, testcase := range tests {
		t.Run(testcase.name, func(t *testing.T) {

			rr := httptest.NewRecorder()
			req, err := http.NewRequest(testcase.verb, "/foo-bar", bytes.NewReader(testcase.payload))

			if err != nil {
				t.Fatal(err)
			}

			buildQuery(req, testcase.pairs)
			handler.ServeHTTP(rr, req)

			// Check the status code is what we expect.
			if status := rr.Code; status != testcase.expectedCode {
				t.Errorf("handler returned wrong status code: got %v want %v - %s", status, testcase.expectedCode, rr.Body)
			}

		})
	}

}
