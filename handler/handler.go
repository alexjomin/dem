package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/alexjomin/dem/geospatial"
	"gitlab.com/alexjomin/dem/geospatial/elevation"
)

// todo
// truncate lon / lat see https://stackoverflow.com/questions/7167604/how-accurately-should-i-store-latitude-and-longitude

type response struct {
	Elevation int16 `json:"elevation"`
}

const (
	// ResponseHeaderContentTypeKey is the key used for response content type
	ResponseHeaderContentTypeKey = "Content-Type"
	// ResponseHeaderContentTypeJSONUTF8 is the key used for UTF8 JSON response
	ResponseHeaderContentTypeJSONUTF8 = "application/json; charset=UTF-8"
)

// JSON Outputs a JSON
func JSON(w http.ResponseWriter, d interface{}) {
	JSONWithHTTPCode(w, d, http.StatusOK)
}

// JSONError sends error with a custom message and error code
func JSONError(w http.ResponseWriter, m string, code int) {
	type errorPayload struct {
		Message string `json:"message"`
	}
	JSONWithHTTPCode(w, errorPayload{m}, code)
}

// JSONWithHTTPCode Json Output with an HTTP code
func JSONWithHTTPCode(w http.ResponseWriter, d interface{}, code int) {
	w.Header().Set(ResponseHeaderContentTypeKey, ResponseHeaderContentTypeJSONUTF8)
	w.WriteHeader(code)
	if d != nil {
		err := json.NewEncoder(w).Encode(d)
		if err != nil {
			// panic will cause the http.StatusInternalServerError to be send to users
			panic(err)
		}
	}
}

// GetJSONContent returns the JSON content of a request
func GetJSONContent(v interface{}, r *http.Request) error {
	if r.Body == nil {
		return errors.New("body is empty")
	}
	err := json.NewDecoder(r.Body).Decode(v)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	return nil
}

type API struct {
	// The bounding box applied
	Bbox           *geospatial.Bbox
	ElevationModel elevation.Model
}

// GetElevation returns the elevation for a given latitude and longitude
func (api *API) GetElevation(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		JSONError(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	slon := r.URL.Query()["lon"]

	if len(slon) == 0 {
		JSONError(w, "Longitude is mandatory", http.StatusBadRequest)
		return
	}

	lon, err := convertToFloat(slon[0])

	if err != nil {
		JSONError(w, "Longitude is not valid", http.StatusBadRequest)
		return
	}

	slat := r.URL.Query()["lat"]

	if len(slat) == 0 {
		JSONError(w, "Latitude is mandatory", http.StatusBadRequest)
		return
	}

	lat, err := convertToFloat(slat[0])

	if err != nil {
		JSONError(w, "Latitude is not valid", http.StatusBadRequest)
		return
	}

	// Create a point corresponding to the given longgitude and latitude
	p, err := geospatial.NewPoint(lon, lat)

	if err != nil {
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if !p.Within(api.Bbox) {
		msg := fmt.Sprintf("Outside of current bounding box - %s", api.Bbox)
		JSONError(w, msg, http.StatusRequestedRangeNotSatisfiable)
		return
	}

	ele, err := api.ElevationModel.ExtractElevationFromLonLat(p)

	if err != nil {
		JSONError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := response{
		Elevation: ele,
	}

	JSON(w, resp)
}

// GetBoundingBox returns the current bounding box
func (api *API) GetBoundingBox(w http.ResponseWriter, r *http.Request) {
	JSON(w, api.Bbox)
}

// ElevationProfile returns a list of elevation for the given points
func (api *API) ElevationProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		JSONError(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	points := geospatial.WayPoints{}
	err := GetJSONContent(&points, r)
	if err != nil {
		JSONError(w, "Payload is not valid", http.StatusBadRequest)
		return
	}

	results := []int16{}

	for _, p := range points {
		if !p.Within(api.Bbox) {
			msg := fmt.Sprintf("Outside of current bounding box - %s", api.Bbox)
			JSONError(w, msg, http.StatusRequestedRangeNotSatisfiable)
			return
		}
		ele, err := api.ElevationModel.ExtractElevationFromLonLat(&p)
		if err != nil {
			JSONError(w, err.Error(), http.StatusInternalServerError)
			return
		}
		results = append(results, ele)
	}

	JSON(w, results)
}

// convertToFloat converts a string to a float64
// TODO: duplicate from cmd package
func convertToFloat(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}
