
# dem

[![pipeline status](https://gitlab.com/alexjomin/dem/badges/master/pipeline.svg)](https://gitlab.com/alexjomin/dem/commits/master)

![dem](https://gisgeography.com/wp-content/uploads/2016/04/DEM-Sources-2-678x322.png)

> Visual Representation of a DEM - Image by GIS Geography [gisgeography.com](https://gisgeography.com)

## Description
**Digital Elevation Model Tool**

**dem** is a cli tool to deal with Digital Elevation Model written in Go.

DEMs are used often in geographic information systems as a 3D representation of a terrain's surface, commonly of a planet.

There are a bunch of DEMs available: 
- SRTM
- ASTER
- JAXA’s Global ALOS 3D World
- LIDAR
- etc.

For the SRTM (Shuttle Radar Topography Mission) The elevation models are arranged into tiles, each covering one degree of latitude and one degree of longitude,  named according to their south western corners. 
For example, "n45e006" stretches from 45°N 6°E to 46°N 7°E and "s45w006" from 45°S 6°W to 44°S 5°W

Actually only the SRTMGL1 (SRTM Version 3.0 Global 1 arc second) is implemented. 
1 arc second resolution offer a precision of about 30 meters. For more information see: 
https://earthdata.nasa.gov/learn/articles/nasa-shuttle-radar-topography-mission-srtm-version-3-0-global-1-arc-second-data-released-over-asia-and-australia

Data comes from NASA’s Earth Observing System Data and Information System (EOSDIS)

To obtain a NASA Earthdata Login account, please visit: https://urs.earthdata.nasa.gov/users/new

This purpose of this cli is to provide commands to: 
- Easely download: 
	- A tile from longitude and latitude 
	- A matrix of tiles from a bounding box defined by a North East and a South West points.
- Create a HTTP server to return the elevation in meter:
	- [GET] /srtmgl1/elevation?lon=3.00&lat=50.00\
	- [POST] /srtmgl1/elevations\
		-  ex: curl -X POST https://myhost.com/srtmgl1/elevations -H "Content-Type: application/json" -d '[{"lon":2.71,"lat":50.32}]'



## Install

    go install gitlab.com/alexjomin/dem

## Commands

### `elevation`


##### Example 
    dem elevation  --login=$LOGIN --password=$PASSOWRD -- 3.0004 50.772


##### Description
Returns the elevation in meter corresponding to the given longitude and latitude







##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```





### `help`



##### Description
Help about any command







##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```





### `readme`



##### Description
Generate Readme



##### Flag
```
-h, --help   help for readme
```





##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```





### `serve`


##### Example 
    dem serve --login=$LOGIN --password=$PASSWORD -- 4.5153 50.9584 1.4941 49.7528


##### Description
Create a HTTP server



##### Flag
```
-p, --port string   The port of the HTTP Server (default "8080")
-t, --tool          Start the bounding box tool 
```





##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```





### `tile`


##### Example 
    dem tile --login=$LOGIN --password=$PASSOWRD -- 3.0004 50.772


##### Description
Downloads the tile corresponding on the given longitude and latitude







##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```





### `tiles`


##### Example 
    dem tiles  --login=$LOGIN --password=$PASSOWRD -- 4.71 45.870 4.644 45.847


##### Description
Downloads a matrix of tiles corresponding to the given bounding box







##### Inherited Flag
```
--login string      Login for the srtm service
--model string      Terrain Model used (default "srtmgl1")
--password string   Password for the srtm service
--path string       Path where data will be downloaded (default ".")
```




