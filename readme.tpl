{{with .Rootcmd}}
# {{.name}}

[![pipeline status](https://gitlab.com/alexjomin/dem/badges/master/pipeline.svg)](https://gitlab.com/alexjomin/dem/commits/master)

![dem](https://gisgeography.com/wp-content/uploads/2016/04/DEM-Sources-2-678x322.png)

> Visual Representation of a DEM - Image by GIS Geography [gisgeography.com](https://gisgeography.com)

## Description
**{{.short}}**

{{.description}}
{{end}}

## Install

    go install gitlab.com/alexjomin/dem

## Commands
{{range .Commands}}
### `{{.name}}`

{{if .example}}
##### Example 
    {{.example}}
{{end}}

##### Description
{{.short}}

{{with .cmd}}
{{if .HasAvailableFlags}}
##### Flag
```
{{.LocalFlags.FlagUsages | trim}}```
{{end}}
{{end}}

{{with .cmd}}
{{ if .HasAvailableInheritedFlags}}
##### Inherited Flag
```
{{.InheritedFlags.FlagUsages | trim}}```
{{end}}
{{end}}


{{end}}